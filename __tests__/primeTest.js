const PrimeNumber = require("../src/classes/primeNumber");

describe("Unit test for prime numbers", () => {
    let primeNumber;

    beforeAll(() => {
        primeNumber = new PrimeNumber();        
    });

    afterAll(() => {
        primeNumber = {};
    });

    test("isPrime(5) => [EXPECTED RESULT = true]", () => {
        expect(primeNumber.isPrime(5)).toEqual(true);        
    });

    test("isPrime(15) => [EXPECTED RESULT = false]", () => {
        expect(primeNumber.isPrime(15)).toEqual(false);        
    });

    test("listPrimeNumbers(15) => [EXPECTED RESULT = [ 2, 3, 5, 7, 11, 13 ]]", () => {
        expect(primeNumber.listPrimeNumbers(15)).toEqual([ 2, 3, 5, 7, 11, 13 ]);        
    });

    test("listPrimeNumbers(7) => [EXPECTED RESULT = [ 2, 3, 5, 7 ]]", () => {
        expect(primeNumber.listPrimeNumbers(7)).toEqual([ 2, 3, 5, 7]);        
    });

});
