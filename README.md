# Prime Numbers NodeJS Excercise

- Prime numbers are numbers that have only 2 factors: 1 and themselves
- For example, the first 5 prime numbers are 2, 3, 5, 7, and 11
- See readme of this repo for more information on specific configurations.

## Getting Started

### Specifications

This project was created in order to run with a docker-compose file

You can run the following commands for download and run the application:

```bash
$ git clone git@gitlab.com:idalio.belmar/prime-numbers-nodejs.git
$ cd prime-numbers-nodejs/

$ docker-compose up
```

You should be able to have and output like this:

```console
 [ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47 ]
```
### Environment variables

The application is managing the next environment variables:


| Variable Name                  | Description                                                                  | Default Value |
| :-------------------------     | :--------------------------------------------------------------------------- | :------------ |
| MAX_PRIME_NUMBER               | Application will return prime numbers from 1 to the value setted in this var |      50       |


You can overwrite `MAX_PRIME_NUMBER` value with the next command :

```bash
$ docker-compose run -e MAX_PRIME_NUMBER=15 nodejs
```
Output:
```console
 [ 2, 3, 5, 7, 11, 13 ]
```

Output wil chance depending the value you're specifing

```bash
$ docker-compose run -e MAX_PRIME_NUMBER=7 nodejs
```
Output:
```console
 [ 2, 3, 5, 7 ]
```

## Author

| Name                      | Email                           |
|---------------------------|-------------------------------- |
| Idalio Belmar Arancibia   | idalio.belmar@gmail.com         |