const PrimeNumber = require("../src/classes/primeNumber");

let primeNumber = new PrimeNumber();

if (isFinite(process.env.MAX_PRIME_NUMBER)) {
  console.log(
    primeNumber.listPrimeNumbers(Math.round(process.env.MAX_PRIME_NUMBER))
  );
} else console.log("Only Numbers accepted");
