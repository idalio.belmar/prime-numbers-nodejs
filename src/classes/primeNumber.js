class PrimeNumber {
  constructor() {}

  /**
   * Retrieve true or false if a number is prime
   * @param {Number} number
   * @returns {Boolean}
   */
  isPrime(number) {
    var divisor = 2;
    while (number > divisor) {
      if (number % divisor == 0) {
        return false;
      } else divisor++;
    }
    return true;
  }

  /**
   * return prime numbers list with 'max'
   * as higher prime number
   * @param {Number} max
   * @returns {Number[]}
   */
  listPrimeNumbers(max) {
    var primeNumbers = [];
    for (let i = 2; i <= max; i++) {
      if (this.isPrime(i)) {
        primeNumbers.push(i);
      }
    }
    return primeNumbers;
  }
}

module.exports = PrimeNumber;
